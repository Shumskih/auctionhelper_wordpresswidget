<?php
/**
* Plugin Name: AuctionHelper
* Description: Retrieve real-time data from AuctionHelper
*/
class AuctionHelper extends WP_Widget {
	function AuctionHelper() {
		parent::WP_Widget(false, "AuctionHelper");
	}

	function widget($args, $instance) {
		$db = new mysqli("localhost", "root", "root", "auction");
		$query = "SELECT count(bidderid) FROM bidders";
		$result = $db->query($query);
		$row = $result->fetch_array();
		$bidders = $row[0];

		$query = "SELECT count(itemid) FROM items";
		$result = $db->query($query);
		$row = $result->fetch_array();
		$items = $row[0];

		$query = "SELECT sum(resaleprice) FROM items";
		$result = $db->query($query);
		$row = $result->fetch_array();
		$totprice = $row[0];

		echo "<h2>Auction Totals</h2><hr>\n";
		echo "Registered bidders: $bidders<br>\n";
		echo "Total Items: $items<br>\n";
		echo "Items resale value: $$totprice<br>\n";
		echo "<hr><br>\n";
	}
}

function register_AuctionHelper() {
	register_widget('AuctionHelper');
}

add_action("widgets_init", "register_AuctionHelper");